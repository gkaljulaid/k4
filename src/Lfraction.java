import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
   }

   private final long numerator;
   private final long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("Denominator cannot be 0.");
      }

      if (a == 0) {
         b = 1;
      }

      if (b < 0) {
         a = -a;
         b = -b;
      }

      long commonDivisor = greatestCommonDivisor(a, b);
      numerator = a / commonDivisor;
      denominator = b / commonDivisor;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m instanceof Lfraction) {
         Lfraction fraction = (Lfraction)m;
         return fraction.getNumerator() == getNumerator() && fraction.getDenominator() == getDenominator();
      }
      return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      long a = getNumerator();
      long b = getDenominator();
      return Objects.hash(a, b);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long a, b;

      if (m.getDenominator() == getDenominator()) {
         a = m.getNumerator() + getNumerator();
         b = getDenominator();
      } else {
         a = m.getNumerator() * getDenominator() + getNumerator() * m.getDenominator();
         b = m.getDenominator() * getDenominator();
      }

      return findReducedFraction(a, b);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long a, b;

      a = m.getNumerator() * getNumerator();
      b = m.getDenominator() * getDenominator();

      return findReducedFraction(a, b);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (getNumerator() == 0) {
         throw new RuntimeException("Cannot inverse fraction because current numerator is 0.");
      }

      return findReducedFraction(getDenominator(), getNumerator());
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return findReducedFraction(-getNumerator(), getDenominator());
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long a, b;

      a = getNumerator() * m.getDenominator() - m.getNumerator() * getDenominator();
      b = m.getDenominator() * getDenominator();

      return findReducedFraction(a, b);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getNumerator() == 0) {
         throw new RuntimeException("Cannot divide by " + m + " because its numerator is 0.");
      }
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long convertedNumeratorThis = getNumerator() * m.getDenominator();
      long convertedNumeratorM = m.getNumerator() * getDenominator();

      if (convertedNumeratorThis < convertedNumeratorM) {
         return -1;
      } else if (convertedNumeratorThis > convertedNumeratorM) {
         return 1;
      }
      return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return Math.floorDiv(getNumerator(), getDenominator());
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(getNumerator() % getDenominator(), getDenominator());
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)getNumerator() / (double)getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] split = s.split("/");
      long numerator, denominator;

      if (split.length > 2) {
         throw new RuntimeException("Too many fraction symbols in provided input: " + s);
      }

      if (split.length < 2) {
         throw new RuntimeException("Too few fraction symbols in provided input: " + s);
      }

      try {
         numerator = Long.parseLong(split[0]);
      } catch (Exception e) {
         throw new RuntimeException(split[0] + " is not a valid numerator in provided input: " + s);
      }
      try {
         denominator = Long.parseLong(split[1]);
      } catch (Exception e) {
         throw new RuntimeException(split[1] + " is not a valid denominator in provided input: " + s);
      }


      return new Lfraction(numerator, denominator);
   }

   private static Lfraction findReducedFraction(long a, long b) {
      long commonDivisor = greatestCommonDivisor(a, b);
      return new Lfraction(a / commonDivisor, b / commonDivisor);
   }

   private static long greatestCommonDivisor(long a, long b) {
      long smaller = Math.abs(Math.min(a, b));
      long bigger = Math.abs(Math.max(a, b));

      for (long i = smaller; i > 1; i--) {
         if (bigger % i == 0 && smaller % i == 0) {
            return i;
         }
      }

      return 1;
   }

}
